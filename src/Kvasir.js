import Vault from "#/vault";
import Vue from "vue";
import { copy_to_clipboard, generate_password } from "#/util";
import QrCode from "qrcode-reader";
import * as url from "url";

export default class Kvasir {
    constructor() {
        this.state = {
            logged_in: false,
            vault_url: "",
            vault_token: "",
            password_db: {},
            current_folder: {
                passwords: []
            },
            otp_codes: [],
            path: [],
            search_query: "",
            active_password: undefined,
            active_code: undefined,
            new_otp_code: undefined,
            show_password: false,
            password_length: 12,
            alpha: true,
            numeric: true,
            symbols: true,
            more_symbols: false,
            length_too_short: false,
            page: 'password_vault'
        };
    }

    init() {
        let vault_url = localStorage.getItem("__pegasus_vault_url");
        if (vault_url) {
            this.state.vault_url = vault_url;

            let vault_token = localStorage.getItem("__pegasus_vault_token");
            if (vault_token) {
                this.state.vault_token = vault_token;
                this.vault = new Vault(vault_url);
                this.vault.token = vault_token;
                this.state.logged_in = true;

                this.vault.check_token().then(() => {
                    this.load_passwords();
                    this.load_otp_codes();
                }).catch(() => {
                    localStorage.removeItem("__pegasus_vault_token");

                    this.vault = undefined;
                    this.state.logged_in = false;
                });
            }
        }

    }

    navigate(page) {
        this.state.page = page;
    }

    test_url(url) {
        return Vault.test_url(url);
    }

    set_url(url) {
        this.state.vault_url = url;
    }

    login(username, password) {
        this.vault = new Vault(this.state.vault_url);
        localStorage.setItem("__pegasus_vault_url", this.state.vault_url);

        return this.vault.login(username, password).then(() => {
            this.state.logged_in = true;
            this.state.vault_token = this.vault.token;

            localStorage.setItem("__pegasus_vault_token", this.state.vault_token);
            this.load_passwords();
        });
    }

    load_passwords() {
        let load_folder = (path) => {
            return this.vault.list(path).then((listing) => {
                let folder = {
                    folders: {},
                    passwords: []
                };

                listing.data.data.keys.forEach((key) => {
                    if (key.endsWith('/')) {
                        let canonical_key = key.replace('/', '');
                        load_folder(`${path}/${canonical_key}`).then((new_folder) => {
                            Vue.set(folder.folders, canonical_key, new_folder);
                        });
                    } else {
                        folder.passwords.push(key);
                    }
                });

                return folder;
            });
        }

        load_folder('passwords').then((passwords) => {
            Vue.set(this.state.password_db, 'passwords', passwords);
            this.state.current_folder = this.state.password_db.passwords;
        });
    }

    load_otp_codes() {
        this.vault.list('totp/keys').then((listing) => {
            Vue.set(this.state.password_db, 'otp_codes', listing.data.data.keys);
            this.state.otp_codes = this.state.password_db.otp_codes;
        });
    }

    open_folder(folder) {
        this.state.current_folder = this.state.current_folder.folders[folder];
        this.state.path.push(folder);
    }

    navigate_up() {
        this.state.path.pop();
        this.navigate_to_path();
    }

    navigate_to_path() {
        this.state.current_folder = this.state.password_db.passwords;
        this.state.path.forEach((folder) => {
            this.state.current_folder = this.state.current_folder.folders[folder];
        });
    }

    get_password_path(password) {
        let path = "";

        if (this.state.path.length > 0) {
            path = `${this.state.path.join('/')}/`;
        }

        return `passwords/${path}${password}`;
    }

    get_password(password) {
        return this.vault.read(this.get_password_path(password));
    }

    show_password(password) {
        this.get_password(password).then((response) => {
            this.state.active_password = response.data.data;
            this.state.active_password.name = password;
        });
    }

    add_password() {
        this.state.active_password = {
            name: "",
            username: "",
            password: ""
        }
        this.generate_password();
    }

    generate_password() {
        if (this.state.password_length < 6) {
            this.state.length_too_short = true;
            return;
        }

        this.state.length_too_short = false;
        let password = generate_password(this.state.password_length, {
            alpha: this.state.alpha,
            numeric: this.state.numeric,
            symbols: this.state.symbols,
            more_symbols: this.state.more_symbols
        });

        this.state.active_password.password = password;
    }

    save_password() {
        let path = this.get_password_path(this.state.active_password.name);
        let data = {
            username: this.state.active_password.username,
            password: this.state.active_password.password
        };


        this.vault.write(path, data).then(() => {
            if (!this.state.current_folder.passwords.includes(this.state.active_password.name)) {
                this.state.current_folder.passwords.push(this.state.active_password.name);
            }

            this.password_back();
        });
    }

    delete_password() {
        let path = this.get_password_path(this.state.active_password.name);

        this.vault.delete(path).then(() => {
            this.state.current_folder.passwords = this.state.current_folder.passwords.filter((password) => {
                return password !== this.state.active_password.name;
            });

            this.password_back();
        });
    }

    copy_password(password, event) {
        this.get_password(password).then((response) => {
            copy_to_clipboard(response.data.data.password);
            window.setTimeout(() => copy_to_clipboard(""), 30000);
        });

        event.preventDefault();
        event.stopPropagation();
    }

    password_back() {
        this.state.active_password = undefined;
    }

    read_code(code_name) {
        return this.vault.read(`totp/code/${code_name}`);
    }

    copy_code(code_name, event) {
        this.read_code(code_name).then((response) => {
            copy_to_clipboard(response.data.data.code);
        });

        event.preventDefault();
        event.stopPropagation();
    }

    show_code(code_name) {
        let promises = [
            this.read_code(code_name),
            this.vault.read(`totp/keys/${code_name}`)
        ];

        Promise.all(promises).then((results) => {
            // This lovely code is to work around an apparent bug with `Vue.set`
            this.state.active_code = Object.assign(results[1].data.data, {
                name: code_name,
                code: results[0].data.data.code,
                age: 0
            });

            this.update_code_age();
        });
    }

    update_code_age() {
        if (this.state.active_code) {
            let age = Math.floor(new Date().getTime() / 1000) % 30;
            this.state.active_code.age = age;
            if (age === 0) {
                this.read_code(this.state.active_code.name).then((response) => {
                    if (this.state.active_code) {
                        this.state.active_code.code = response.data.data.code;

                        setTimeout(() => this.update_code_age(), 1000);
                    }
                });
            } else {
                setTimeout(() => this.update_code_age(), 1000);
            }
        }
    }

    code_back() {
        this.state.active_code = false;
    }

    delete_code() {
        this.vault.delete(`totp/keys/${this.state.active_code.name}`).then(() => {
            this.state.otp_codes = this.state.otp_codes.filter((code) => {
                return code !== this.state.active_code.name;
            });

            this.state.active_code = undefined;
        });
    }

    search() {
        if (this.state.search_query.length > 0) {
            let flat_passwords = [];
            let append_folder = (path, folder) => {
                folder.passwords.forEach((password) => {
                    flat_passwords.push(`${path}${password}`);
                });

                if (folder.folders) {
                    Object.keys(folder.folders).forEach((folder_name) => {
                        append_folder(`${path}${folder_name}/`, folder.folders[folder_name]);
                    });
                }
            };

            append_folder("", this.state.password_db.passwords);

            let regex = new RegExp(this.state.search_query);

            this.state.current_folder = {
                passwords: flat_passwords.filter((password) => regex.test(password))
            };

            this.state.otp_codes = this.state.password_db.otp_codes.filter((code_name) => regex.test(code_name));
        } else {
            this.navigate_to_path();
            this.state.otp_codes = this.state.password_db.otp_codes;
        }
    }

    add_otp() {
        browser.tabs.captureVisibleTab().then((image) => {
            let qr = new QrCode();
            qr.callback = (error, result) => {
                document.body.style.display = "block";
                if (error) {
                    console.error("Error parsing QR code", error);
                } else {
                    let otp_url = url.parse(result.result, true);
                    if (!otp_url) {
                        return;
                    }
                    let account_name = decodeURIComponent(otp_url.pathname.slice(1));
                    let issuer = otp_url.query.issuer;
                    let name = issuer;

                    this.state.new_otp_code = {
                        account_name,
                        issuer,
                        name,
                        url: result.result
                    };
                }
            };

            document.body.style.display = "none";
            qr.decode(image);
        });
    }

    cancel_new_otp() {
        this.state.new_otp_code = undefined;
    }

    save_new_otp() {
        this.vault.write(`totp/keys/${this.state.new_otp_code.name}`, {url: this.state.new_otp_code.url}).then(() => {
            this.state.otp_codes.push(this.state.new_otp_code.name)
            this.state.new_otp_code = undefined;
        });
    }
}