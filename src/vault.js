import * as axios from "axios";

export default class Vault {
    constructor(url) {
        this.client = axios.create({
            baseURL: `${url}/v1`
        });
    }

    write(url, data) {
        return this.client.post(url, data);
    }

    read(url) {
        return this.client.get(url);
    }

    list(url) {
        return this.client.request({
            method: "list",
            url: url
        });
    }

    delete(url) {
        return this.client.request({
            method: "delete",
            url: url
        });
    }

    login(username, password) {
        return this.write(`/auth/userpass/login/${username}`, {password}).then((response) => {
            this.token = response.data.auth.client_token;
        });
    }

    check_token() {
        return this.read('/auth/token/lookup-self');
    }

    set token(token) {
        this.client.defaults.headers.common["X-Vault-Token"] = token;
    }

    get token() {
        return this.client.defaults.headers.common["X-Vault-Token"];
    }

    static test_url(url) {
        let client = new Vault(url);

        return client.read("/sys/health?standbyok").then((response) => {
            return true;
        }).catch(() => {
            return false;
        });
    }
}